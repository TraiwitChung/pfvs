clc
clear
mrstModule add agmg incomp agglom coarsegrid libgeometry;
startup;


R = 10e-6/1;
load sandpack.mat
sample = sandpack;
rho = 1;
mu = 1;
P1 = 1;
P2 = 0;
Shape = 1;
sigm = 1;
trueperm = 1;

Ax = size(sample,1);
Ay = size(sample,2);
Az = size(sample,3);

        
%%
sample = connectivitycheck(sample);
distgeo = double(bwdist(sample,'euclidean'));
maxdist = max(distgeo(:));
Niteration = size(unique(distgeo),1);

NconnectporeOG = sum(sample(:)==0);

%[Dmax] = calcDmax(distgeo,24)
[Dmax] = DmaxCalc(distgeo,maxdist,Ax,Ay,Az,Shape,R,mu);
w =  (Shape) * R^2 * (1/(8*mu)) .* (2 .* (Dmax .* distgeo) - (distgeo).^2 );
w = w;
Nconnectpore = sum(Dmax(:)>0);
rc = unique(Dmax);
rc = flipud(rc);
num = size(rc,1);
K = zeros(num,3);

%% making G
nx = Ax; ny = Ay; nz = Az;
G = cartGrid([nx, ny, nz], [nx, ny, nz]);
%%
for ii = 1:2:size(rc,1)
    
    newgeo = ones(Ax,Ay,Az);
    newgeo2 = ones(Ax,Ay,Az);
    ii
    temp = rc(ii);
    % 3 phase segmentation
    newgeo(Dmax > temp) = 0;  %NWP
    newgeo2(Dmax <= temp & Dmax ~= 0) = 0;  %WP
    
   Sw = sum(newgeo2(:)==0) /NconnectporeOG 
    
   newgeo = connectivitycheck(newgeo);
   newgeo2 = connectivitycheck(newgeo2);
    
   
%    
%% Only calculate k when both phases are hydraulically connected from inlet to outlet


 if sum(newgeo2(:)==0) > 1  & sum(newgeo(:)==0) > 1   %water perm
   
   permW = TwoPhasePFVS(newgeo2,R,P1,P2,Shape,rho,mu,G,w)
   permO = TwoPhasePFVS(newgeo,R,P1,P2,Shape,rho,mu,G,w)
   else
        permO = 0;
        permW = 0;
   end


      %%
      
   K(ii,:) = [Sw,permW,permO];
   
end


KK = K;
KK(all(~KK,2),:) = [];
%%
Sw = KK(:,1);
Kwp =KK(:,2)
Knwp = KK(:,3)
Sw = Sw(Kwp>0);
Kwp = Kwp(Kwp>0);
Knwp = Knwp(Knwp>0);
%%
figure(1)
plot(Sw, Kwp)
hold on
plot(Sw, Knwp)
title('Relative Permeability', 'FontSize',15)
xlabel('Sw'), ylabel('Kr');
 set(gcf,'color','w');
