function [ Dmax ] = DmaxCalc( D, maxdist,Ax, Ay, Az, Shape,R,mu )
% this is the algorithm to indentify the local maximum flow channel.
% First, let Dmax = D then check all the neighboring voxels, if any of
% the neighboring voxels has higher Dmax then update the current Dmax to
% that value, if not Dmax stays the same.


%define initial Dmax

Dmax = zeros(Ax,Ay,Az);
Dmax = D;
w =  zeros(Ax,Ay,Az);

  for t = 1: round(maxdist)  +1
      
        progress = 100*(t/(round(maxdist)+2));
        progress = num2str(progress);
        fprintf('PROGRESS at %s PERCENT.\n',progress);
      
     
        for i = 2:Az-1
         for j = 2:Ay-1
          for k = 2:Ax-1
            
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i+1,j,k)
          if Dmax(i,j,k) < Dmax(i+1,j,k)
          Dmax(i,j,k) = Dmax(i+1,j,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j,k)
          if Dmax(i,j,k) < Dmax(i-1,j,k)
              Dmax(i,j,k) = Dmax(i-1,j,k);
          end
      end 
          
      if D(i,j,k) < D(i,j+1,k)
          if Dmax(i,j,k) < Dmax(i,j+1,k)
              Dmax(i,j,k) = Dmax(i,j+1,k);
          end
      end 
            
      if D(i,j,k) < D(i,j-1,k)
          if Dmax(i,j,k) < Dmax(i,j-1,k)
              Dmax(i,j,k) = Dmax(i,j-1,k);
          end
      end
              
      if D(i,j,k) < D(i,j,k+1)
          if Dmax(i,j,k) < Dmax(i,j,k+1)
              Dmax(i,j,k) = Dmax(i,j,k+1);
          end
      end
              
      if D(i,j,k) < D(i,j,k-1)
          if Dmax(i,j,k) < Dmax(i,j,k-1)
             Dmax(i,j,k) = Dmax(i,j,k-1);
          end
      end
      
      
      if D(i,j,k) < D(i-1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k+1);
          end
      end
      
         if D(i,j,k) < D(i,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i,j+1,k+1)
              Dmax(i,j,k) = Dmax(i,j+1,k+1);
          end
      end
      
       if D(i,j,k) < D(i+1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k+1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j,k+1)
              Dmax(i,j,k) = Dmax(i-1,j,k+1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j,k+1)
              Dmax(i,j,k) = Dmax(i+1,j,k+1);
          end
       end
      
        if D(i,j,k) < D(i-1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i,j-1,k+1)
              Dmax(i,j,k) = Dmax(i,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i+1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i-1,j+1,k)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k)
          Dmax(i,j,k) = Dmax(i-1,j+1,k);
          end
        end
      
      if D(i,j,k) < D(i+1,j+1,k)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k)
          Dmax(i,j,k) = Dmax(i+1,j+1,k);
          end
      end
      
      if D(i,j,k) < D(i+1,j-1,k)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k)
          Dmax(i,j,k) = Dmax(i+1,j-1,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j-1,k)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k)
          Dmax(i,j,k) = Dmax(i-1,j-1,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k-1);
          end
      end
      
         if D(i,j,k) < D(i,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i,j+1,k-1)
              Dmax(i,j,k) = Dmax(i,j+1,k-1);
          end
          end
      
       if D(i,j,k) < D(i+1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k-1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j,k-1)
              Dmax(i,j,k) = Dmax(i-1,j,k-1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j,k-1)
              Dmax(i,j,k) = Dmax(i+1,j,k-1);
          end
       end
      
        if D(i,j,k) < D(i-1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i,j-1,k-1)
              Dmax(i,j,k) = Dmax(i,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i+1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k-1);
          end
        end
      
                    end
                   end
                  end
        end
   
%face k = 1

for i = 2:Ax-1
         for j = 2:Ay-1
             
            k = 1;
            
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i+1,j,k)
          if Dmax(i,j,k) < Dmax(i+1,j,k)
          Dmax(i,j,k) = Dmax(i+1,j,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j,k)
          if Dmax(i,j,k) < Dmax(i-1,j,k)
              Dmax(i,j,k) = Dmax(i-1,j,k);
          end
      end 
          
      if D(i,j,k) < D(i,j+1,k)
          if Dmax(i,j,k) < Dmax(i,j+1,k)
              Dmax(i,j,k) = Dmax(i,j+1,k);
          end
      end 
            
      if D(i,j,k) < D(i,j-1,k)
          if Dmax(i,j,k) < Dmax(i,j-1,k)
              Dmax(i,j,k) = Dmax(i,j-1,k);
          end
      end
              
      if D(i,j,k) < D(i,j,k+1)
          if Dmax(i,j,k) < Dmax(i,j,k+1)
              Dmax(i,j,k) = Dmax(i,j,k+1);
          end
      end
              
     
      
      
      if D(i,j,k) < D(i-1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k+1);
          end
      end
      
         if D(i,j,k) < D(i,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i,j+1,k+1)
              Dmax(i,j,k) = Dmax(i,j+1,k+1);
          end
      end
      
       if D(i,j,k) < D(i+1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k+1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j,k+1)
              Dmax(i,j,k) = Dmax(i-1,j,k+1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j,k+1)
              Dmax(i,j,k) = Dmax(i+1,j,k+1);
          end
       end
      
        if D(i,j,k) < D(i-1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i,j-1,k+1)
              Dmax(i,j,k) = Dmax(i,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i+1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i-1,j+1,k)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k)
          Dmax(i,j,k) = Dmax(i-1,j+1,k);
          end
        end
      
      if D(i,j,k) < D(i+1,j+1,k)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k)
          Dmax(i,j,k) = Dmax(i+1,j+1,k);
          end
      end
      
      if D(i,j,k) < D(i+1,j-1,k)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k)
          Dmax(i,j,k) = Dmax(i+1,j-1,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j-1,k)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k)
          Dmax(i,j,k) = Dmax(i-1,j-1,k);
          end
      end
  
                    end
                   end
end
                  

%face k = end

for i = 2:Ax-1
         for j = 2:Ay-1
             
            k = Az;
            
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i+1,j,k)
          if Dmax(i,j,k) < Dmax(i+1,j,k)
          Dmax(i,j,k) = Dmax(i+1,j,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j,k)
          if Dmax(i,j,k) < Dmax(i-1,j,k)
              Dmax(i,j,k) = Dmax(i-1,j,k);
          end
      end 
          
      if D(i,j,k) < D(i,j+1,k)
          if Dmax(i,j,k) < Dmax(i,j+1,k)
              Dmax(i,j,k) = Dmax(i,j+1,k);
          end
      end 
            
      if D(i,j,k) < D(i,j-1,k)
          if Dmax(i,j,k) < Dmax(i,j-1,k)
              Dmax(i,j,k) = Dmax(i,j-1,k);
          end
      end
              
      if D(i,j,k) < D(i,j,k-1)
          if Dmax(i,j,k) < Dmax(i,j,k-1)
              Dmax(i,j,k) = Dmax(i,j,k-1);
          end
      end
              
     
      
      
      if D(i,j,k) < D(i-1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k-1);
          end
      end
      
         if D(i,j,k) < D(i,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i,j+1,k-1)
              Dmax(i,j,k) = Dmax(i,j+1,k-1);
          end
      end
      
       if D(i,j,k) < D(i+1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k-1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j,k-1)
              Dmax(i,j,k) = Dmax(i-1,j,k-1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j,k-1)
              Dmax(i,j,k) = Dmax(i+1,j,k-1);
          end
       end
      
        if D(i,j,k) < D(i-1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i,j-1,k-1)
              Dmax(i,j,k) = Dmax(i,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i+1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i-1,j+1,k)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k)
          Dmax(i,j,k) = Dmax(i-1,j+1,k);
          end
        end
      
      if D(i,j,k) < D(i+1,j+1,k)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k)
          Dmax(i,j,k) = Dmax(i+1,j+1,k);
          end
      end
      
      if D(i,j,k) < D(i+1,j-1,k)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k)
          Dmax(i,j,k) = Dmax(i+1,j-1,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j-1,k)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k)
          Dmax(i,j,k) = Dmax(i-1,j-1,k);
          end
      end
  
                    end
                   end
end
       
                  
%face j = 1

  for i = 2:Ax-1
         
            for k = 2:Az-1
                
                j = 1;
            
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i+1,j,k)
          if Dmax(i,j,k) < Dmax(i+1,j,k)
          Dmax(i,j,k) = Dmax(i+1,j,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j,k)
          if Dmax(i,j,k) < Dmax(i-1,j,k)
              Dmax(i,j,k) = Dmax(i-1,j,k);
          end
      end 
          
      if D(i,j,k) < D(i,j+1,k)
          if Dmax(i,j,k) < Dmax(i,j+1,k)
              Dmax(i,j,k) = Dmax(i,j+1,k);
          end
      end 
            
     
              
      if D(i,j,k) < D(i,j,k+1)
          if Dmax(i,j,k) < Dmax(i,j,k+1)
              Dmax(i,j,k) = Dmax(i,j,k+1);
          end
      end
              
      if D(i,j,k) < D(i,j,k-1)
          if Dmax(i,j,k) < Dmax(i,j,k-1)
             Dmax(i,j,k) = Dmax(i,j,k-1);
          end
      end
      
      
      if D(i,j,k) < D(i-1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k+1);
          end
      end
      
         if D(i,j,k) < D(i,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i,j+1,k+1)
              Dmax(i,j,k) = Dmax(i,j+1,k+1);
          end
      end
      
       if D(i,j,k) < D(i+1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k+1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j,k+1)
              Dmax(i,j,k) = Dmax(i-1,j,k+1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j,k+1)
              Dmax(i,j,k) = Dmax(i+1,j,k+1);
          end
       end
      
        
       
        if D(i,j,k) < D(i-1,j+1,k)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k)
          Dmax(i,j,k) = Dmax(i-1,j+1,k);
          end
        end
      
      if D(i,j,k) < D(i+1,j+1,k)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k)
          Dmax(i,j,k) = Dmax(i+1,j+1,k);
          end
      end
      
     
      
      if D(i,j,k) < D(i-1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k-1);
          end
      end
      
         if D(i,j,k) < D(i,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i,j+1,k-1)
              Dmax(i,j,k) = Dmax(i,j+1,k-1);
          end
          end
      
       if D(i,j,k) < D(i+1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k-1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j,k-1)
              Dmax(i,j,k) = Dmax(i-1,j,k-1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j,k-1)
              Dmax(i,j,k) = Dmax(i+1,j,k-1);
          end
       end
      
                    end
                   end
                  end
      
%face j = end

  for i = 2:Ax-1
       
  for k = 2:Az-1
                
              j = Ay;
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i+1,j,k)
          if Dmax(i,j,k) < Dmax(i+1,j,k)
          Dmax(i,j,k) = Dmax(i+1,j,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j,k)
          if Dmax(i,j,k) < Dmax(i-1,j,k)
              Dmax(i,j,k) = Dmax(i-1,j,k);
          end
      end 
          
      if D(i,j,k) < D(i,j-1,k)
          if Dmax(i,j,k) < Dmax(i,j-1,k)
              Dmax(i,j,k) = Dmax(i,j-1,k);
          end
      end 
            
     
              
      if D(i,j,k) < D(i,j,k+1)
          if Dmax(i,j,k) < Dmax(i,j,k+1)
              Dmax(i,j,k) = Dmax(i,j,k+1);
          end
      end
              
      if D(i,j,k) < D(i,j,k-1)
          if Dmax(i,j,k) < Dmax(i,j,k-1)
             Dmax(i,j,k) = Dmax(i,j,k-1);
          end
      end
      
      
      if D(i,j,k) < D(i-1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k+1);
          end
      end
      
         if D(i,j,k) < D(i,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i,j-1,k+1)
              Dmax(i,j,k) = Dmax(i,j-1,k+1);
          end
      end
      
       if D(i,j,k) < D(i+1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k+1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j,k+1)
              Dmax(i,j,k) = Dmax(i-1,j,k+1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j,k+1)
              Dmax(i,j,k) = Dmax(i+1,j,k+1);
          end
       end
      
        
       
        if D(i,j,k) < D(i-1,j-1,k)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k)
          Dmax(i,j,k) = Dmax(i-1,j-1,k);
          end
        end
      
      if D(i,j,k) < D(i+1,j-1,k)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k)
          Dmax(i,j,k) = Dmax(i+1,j-1,k);
          end
      end
      
     
      
      if D(i,j,k) < D(i-1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k-1);
          end
      end
      
         if D(i,j,k) < D(i,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i,j-1,k-1)
              Dmax(i,j,k) = Dmax(i,j-1,k-1);
          end
          end
      
       if D(i,j,k) < D(i+1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k-1);
          end
       end
      
       if D(i,j,k) < D(i-1,j,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j,k-1)
              Dmax(i,j,k) = Dmax(i-1,j,k-1);
          end
       end
      
       if D(i,j,k) < D(i+1,j,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j,k-1)
              Dmax(i,j,k) = Dmax(i+1,j,k-1);
          end
       end
      
                    end
                   end
  end
                  
  
%face i = 1

         for j = 2:Ay-1
            for k = 2:Az-1
                
                i = 1;
            
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i+1,j,k)
          if Dmax(i,j,k) < Dmax(i+1,j,k)
          Dmax(i,j,k) = Dmax(i+1,j,k);
          end
      end
      
     
          
      if D(i,j,k) < D(i,j+1,k)
          if Dmax(i,j,k) < Dmax(i,j+1,k)
              Dmax(i,j,k) = Dmax(i,j+1,k);
          end
      end 
            
      if D(i,j,k) < D(i,j-1,k)
          if Dmax(i,j,k) < Dmax(i,j-1,k)
              Dmax(i,j,k) = Dmax(i,j-1,k);
          end
      end
              
      if D(i,j,k) < D(i,j,k+1)
          if Dmax(i,j,k) < Dmax(i,j,k+1)
              Dmax(i,j,k) = Dmax(i,j,k+1);
          end
      end
              
      if D(i,j,k) < D(i,j,k-1)
          if Dmax(i,j,k) < Dmax(i,j,k-1)
             Dmax(i,j,k) = Dmax(i,j,k-1);
          end
      end
      
      
      
      
         if D(i,j,k) < D(i,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i,j+1,k+1)
              Dmax(i,j,k) = Dmax(i,j+1,k+1);
          end
      end
      
       if D(i,j,k) < D(i+1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k+1);
          end
       end
      
    
      
       if D(i,j,k) < D(i+1,j,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j,k+1)
              Dmax(i,j,k) = Dmax(i+1,j,k+1);
          end
       end
      
       
       
        if D(i,j,k) < D(i,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i,j-1,k+1)
              Dmax(i,j,k) = Dmax(i,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i+1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k+1);
          end
        end
       
     
      if D(i,j,k) < D(i+1,j+1,k)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k)
          Dmax(i,j,k) = Dmax(i+1,j+1,k);
          end
      end
      
      if D(i,j,k) < D(i+1,j-1,k)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k)
          Dmax(i,j,k) = Dmax(i+1,j-1,k);
          end
      end
      
      
      
         if D(i,j,k) < D(i,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i,j+1,k-1)
              Dmax(i,j,k) = Dmax(i,j+1,k-1);
          end
          end
      
       if D(i,j,k) < D(i+1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j+1,k-1);
          end
       end
      
     
      
       if D(i,j,k) < D(i+1,j,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j,k-1)
              Dmax(i,j,k) = Dmax(i+1,j,k-1);
          end
       end
      
       
       
        if D(i,j,k) < D(i,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i,j-1,k-1)
              Dmax(i,j,k) = Dmax(i,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i+1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i+1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i+1,j-1,k-1);
          end
        end
      
                    end
                   end
         end
                  
         
%face i = end

         for j = 2:Ay-1
            for k = 2:Az-1
                
                i = Ax;
            
      if D(i,j,k) ~= 0
          
      if D(i,j,k) < D(i-1,j,k)
          if Dmax(i,j,k) < Dmax(i-1,j,k)
          Dmax(i,j,k) = Dmax(i-1,j,k);
          end
      end
      
     
          
      if D(i,j,k) < D(i,j+1,k)
          if Dmax(i,j,k) < Dmax(i,j+1,k)
              Dmax(i,j,k) = Dmax(i,j+1,k);
          end
      end 
            
      if D(i,j,k) < D(i,j-1,k)
          if Dmax(i,j,k) < Dmax(i,j-1,k)
              Dmax(i,j,k) = Dmax(i,j-1,k);
          end
      end
              
      if D(i,j,k) < D(i,j,k+1)
          if Dmax(i,j,k) < Dmax(i,j,k+1)
              Dmax(i,j,k) = Dmax(i,j,k+1);
          end
      end
              
      if D(i,j,k) < D(i,j,k-1)
          if Dmax(i,j,k) < Dmax(i,j,k-1)
             Dmax(i,j,k) = Dmax(i,j,k-1);
          end
      end
      
      
      
      
         if D(i,j,k) < D(i,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i,j+1,k+1)
              Dmax(i,j,k) = Dmax(i,j+1,k+1);
          end
      end
      
       if D(i,j,k) < D(i-1,j+1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k+1);
          end
       end
      
    
      
       if D(i,j,k) < D(i-1,j,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j,k+1)
              Dmax(i,j,k) = Dmax(i-1,j,k+1);
          end
       end
      
       
       
        if D(i,j,k) < D(i,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i,j-1,k+1)
              Dmax(i,j,k) = Dmax(i,j-1,k+1);
          end
        end
       
        if D(i,j,k) < D(i-1,j-1,k+1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k+1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k+1);
          end
        end
       
     
      if D(i,j,k) < D(i-1,j+1,k)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k)
          Dmax(i,j,k) = Dmax(i-1,j+1,k);
          end
      end
      
      if D(i,j,k) < D(i-1,j-1,k)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k)
          Dmax(i,j,k) = Dmax(i-1,j-1,k);
          end
      end
      
      
      
         if D(i,j,k) < D(i,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i,j+1,k-1)
              Dmax(i,j,k) = Dmax(i,j+1,k-1);
          end
          end
      
       if D(i,j,k) < D(i-1,j+1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j+1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j+1,k-1);
          end
       end
      
     
      
       if D(i,j,k) < D(i-1,j,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j,k-1)
              Dmax(i,j,k) = Dmax(i-1,j,k-1);
          end
       end
      
       
       
        if D(i,j,k) < D(i,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i,j-1,k-1)
              Dmax(i,j,k) = Dmax(i,j-1,k-1);
          end
        end
       
        if D(i,j,k) < D(i-1,j-1,k-1)
          if Dmax(i,j,k) < Dmax(i-1,j-1,k-1)
              Dmax(i,j,k) = Dmax(i-1,j-1,k-1);
          end
        end
      
                    end
                   end
                  end
       
       
  
  
        
  %check i edge
  for i=2:Ax-1
      
      if D(i,j,k) ~= 0
  
    if D(i,1,1) < D(i+1,1,1)
       if Dmax(i,1,1) < Dmax(i+1,1,1)
            Dmax(i,1,1) = Dmax(i+1,1,1);
       end
   end
   
   if D(i,1,1) < D(i-1,1,1)
       if Dmax(i,1,1) < Dmax(i-1,1,1)
            Dmax(i,1,1) = Dmax(i-1,1,1);
       end
   end
   
    if D(i,1,1) < D(i-1,2,1)
       if Dmax(i,1,1) < Dmax(i-1,2,1)
            Dmax(i,1,1) = Dmax(i-1,2,1);
       end
    end
   
     if D(i,1,1) < D(i+1,2,1)
       if Dmax(i,1,1) < Dmax(i+1,2,1)
            Dmax(i,1,1) = Dmax(i+1,2,1);
       end
     end
   
     if D(i,1,1) < D(i,2,1)
       if Dmax(i,1,1) < Dmax(i,2,1)
            Dmax(i,1,1) = Dmax(i,2,1);
       end
     end
   
       if D(i,1,1) < D(i,1,2)
       if Dmax(i,1,1) < Dmax(i,1,2)
            Dmax(i,1,1) = Dmax(i,1,2);
       end
      end
      if D(i,1,1) < D(i+1,1,2)
       if Dmax(i,1,1) < Dmax(i+1,1,2)
            Dmax(i,1,1) = Dmax(i+1,1,2);
       end
   end
   
   if D(i,1,1) < D(i-1,1,2)
       if Dmax(i,1,1) < Dmax(i-1,1,2)
            Dmax(i,1,1) = Dmax(i-1,1,2);
       end
   end
   
    if D(i,1,1) < D(i-1,2,2)
       if Dmax(i,1,1) < Dmax(i-1,2,2)
            Dmax(i,1,1) = Dmax(i-1,2,2);
       end
    end
   
     if D(i,1,1) < D(i+1,2,2)
       if Dmax(i,1,1) < Dmax(i+1,2,2)
            Dmax(i,1,1) = Dmax(i+1,2,2);
       end
     end
   
     if D(i,1,1) < D(i,2,2)
       if Dmax(i,1,1) < Dmax(i,2,2)
            Dmax(i,1,1) = Dmax(i,2,2);
       end
     end
   
 %%
 
     if D(i,end,1) < D(i+1,end,1)
       if Dmax(i,end,1) < Dmax(i+1,end,1)
            Dmax(i,end,1) = Dmax(i+1,end,1);
       end
   end
   
   if D(i,end,1) < D(i-1,end,1)
       if Dmax(i,end,1) < Dmax(i-1,end,1)
            Dmax(i,end,1) = Dmax(i-1,end,1);
       end
   end
   
    if D(i,end,1) < D(i-1,end-1,1)
       if Dmax(i,end,1) < Dmax(i-1,end-1,1)
            Dmax(i,end,1) = Dmax(i-1,end-1,1);
       end
    end
   
     if D(i,end,1) < D(i+1,end-1,1)
       if Dmax(i,end,1) < Dmax(i+1,end-1,1)
            Dmax(i,end,1) = Dmax(i+1,end-1,1);
       end
     end
   
     if D(i,end,1) < D(i,end-1,1)
       if Dmax(i,end,1) < Dmax(i,end-1,1)
            Dmax(i,end,1) = Dmax(i,end-1,1);
       end
     end
   
      if D(i,end,1) < D(i,end,2)
       if Dmax(i,end,1) < Dmax(i,end,2)
            Dmax(i,end,1) = Dmax(i,end,2);
       end
      end
   
      if D(i,end,1) < D(i+1,end,2)
       if Dmax(i,end,1) < Dmax(i+1,end,2)
            Dmax(i,end,1) = Dmax(i+1,end,2);
       end
   end
   
   if D(i,end,1) < D(i-1,end,2)
       if Dmax(i,end,1) < Dmax(i-1,end,2)
            Dmax(i,end,1) = Dmax(i-1,end,2);
       end
   end
   
    if D(i,end,1) < D(i-1,end-1,2)
       if Dmax(i,end,1) < Dmax(i-1,end-1,2)
            Dmax(i,end,1) = Dmax(i-1,end-1,2);
       end
    end
   
     if D(i,end,1) < D(i+1,end-1,2)
       if Dmax(i,end,1) < Dmax(i+1,end-1,2)
            Dmax(i,end,1) = Dmax(i+1,end-1,2);
       end
     end
   
     if D(i,end,1) < D(i,end-1,2)
       if Dmax(i,end,1) < Dmax(i,end-1,2)
            Dmax(i,end,1) = Dmax(i,end-1,2);
       end
     end
   
     %% change in z
     
     if D(i,1,end) < D(i+1,1,end)
       if Dmax(i,1,end) < Dmax(i+1,1,end)
            Dmax(i,1,end) = Dmax(i+1,1,end);
       end
   end
   
   if D(i,1,end) < D(i-1,1,end)
       if Dmax(i,1,end) < Dmax(i-1,1,end)
            Dmax(i,1,end) = Dmax(i-1,1,end);
       end
   end
   
    if D(i,1,end) < D(i-1,2,end)
       if Dmax(i,1,end) < Dmax(i-1,2,end)
            Dmax(i,1,end) = Dmax(i-1,2,end);
       end
    end
   
     if D(i,1,end) < D(i+1,2,end)
       if Dmax(i,1,end) < Dmax(i+1,2,end)
            Dmax(i,1,end) = Dmax(i+1,2,end);
       end
     end
   
     if D(i,1,end) < D(i,2,end)
       if Dmax(i,1,end) < Dmax(i,2,end)
            Dmax(i,1,end) = Dmax(i,2,end);
       end
     end
   
       if D(i,end,end) < D(i,1,end-1)
       if Dmax(i,end,end) < Dmax(i,1,end-1)
            Dmax(i,end,end) = Dmax(i,1,end-1);
       end
      end
      if D(i,1,end) < D(i+1,1,end-1)
       if Dmax(i,1,end) < Dmax(i+1,1,end-1)
            Dmax(i,1,end) = Dmax(i+1,1,end-1);
       end
   end
   
   if D(i,1,end) < D(i-1,1,end-1)
       if Dmax(i,1,end) < Dmax(i-1,1,end-1)
            Dmax(i,1,end) = Dmax(i-1,1,end-1);
       end
   end
   
    if D(i,1,end) < D(i-1,2,end-1)
       if Dmax(i,1,end) < Dmax(i-1,2,end-1)
            Dmax(i,1,end) = Dmax(i-1,2,end-1);
       end
    end
   
     if D(i,1,end) < D(i+1,2,end-1)
       if Dmax(i,1,end) < Dmax(i+1,2,end-1)
            Dmax(i,1,end) = Dmax(i+1,2,end-1);
       end
     end
   
     if D(i,1,end) < D(i,2,end-1)
       if Dmax(i,1,end) < Dmax(i,2,end-1)
            Dmax(i,1,end) = Dmax(i,2,end-1);
       end
     end
   
 %%
 
     if D(i,end,end) < D(i+1,end,end)
       if Dmax(i,end,end) < Dmax(i+1,end,end)
            Dmax(i,end,end) = Dmax(i+1,end,end);
       end
   end
   
   if D(i,end,end) < D(i-1,end,end)
       if Dmax(i,end,end) < Dmax(i-1,end,end)
            Dmax(i,end,end) = Dmax(i-1,end,end);
       end
   end
   
    if D(i,end,end) < D(i-1,end-1,end)
       if Dmax(i,end,end) < Dmax(i-1,end-1,end)
            Dmax(i,end,end) = Dmax(i-1,end-1,end);
       end
    end
   
     if D(i,end,end) < D(i+1,end-1,end)
       if Dmax(i,end,end) < Dmax(i+1,end-1,end)
            Dmax(i,end,end) = Dmax(i+1,end-1,end);
       end
     end
   
     if D(i,end,end) < D(i,end-1,end)
       if Dmax(i,end,end) < Dmax(i,end-1,end)
            Dmax(i,end,end) = Dmax(i,end-1,end);
       end
     end
   
      if D(i,end,end) < D(i,end,end-1)
       if Dmax(i,end,end) < Dmax(i,end,end-1)
            Dmax(i,end,end) = Dmax(i,end,end-1);
       end
      end
   
      if D(i,end,end) < D(i+1,end,end-1)
       if Dmax(i,end,end) < Dmax(i+1,end,end-1)
            Dmax(i,end,end) = Dmax(i+1,end,end-1);
       end
   end
   
   if D(i,end,end) < D(i-1,end,end-1)
       if Dmax(i,end,end) < Dmax(i-1,end,end-1)
            Dmax(i,end,end) = Dmax(i-1,end,end-1);
       end
   end
   
    if D(i,end,end) < D(i-1,end-1,end-1)
       if Dmax(i,end,end) < Dmax(i-1,end-1,end-1)
            Dmax(i,end,end) = Dmax(i-1,end-1,end-1);
       end
    end
   
     if D(i,end,end) < D(i+1,end-1,end-1)
       if Dmax(i,end,end) < Dmax(i+1,end-1,end-1)
            Dmax(i,end,end) = Dmax(i+1,end-1,end-1);
       end
     end
   
     if D(i,end,end) < D(i,end-1,end-1)
       if Dmax(i,end,end) < Dmax(i,end-1,end-1)
            Dmax(i,end,end) = Dmax(i,end-1,end-1);
       end
     end
     
      end
  end
     
  %check j edge
  for j=2:Ay-1
  
       if D(i,j,k) ~= 0
    if D(1,j,1) < D(1,j+1,1)
       if Dmax(1,j,1) < Dmax(1,j+1,1)
            Dmax(1,j,1) = Dmax(1,j+1,1);
       end
   end
   
   if D(1,j,1) < D(1,j-1,1)
       if Dmax(1,j,1) < Dmax(1,j-1,1)
            Dmax(1,j,1) = Dmax(1,j-1,1);
       end
   end
   
    if D(1,j,1) < D(2,j-1,1)
       if Dmax(1,j,1) < Dmax(2,j-1,1)
            Dmax(1,j,1) = Dmax(2,j-1,1);
       end
    end
   
     if D(1,j,1) < D(2,j+1,1)
       if Dmax(1,j,1) < Dmax(2,j+1,1)
            Dmax(1,j,1) = Dmax(2,j+1,1);
       end
     end
   
     if D(1,j,1) < D(2,j,1)
       if Dmax(1,j,1) < Dmax(2,j,1)
            Dmax(1,j,1) = Dmax(2,j,1);
       end
     end
   
       if D(1,j,1) < D(1,j,2)
       if Dmax(1,j,1) < Dmax(1,j,2)
            Dmax(1,j,1) = Dmax(1,j,2);
       end
      end
      if D(1,j,1) < D(1,j+1,2)
       if Dmax(1,j,1) < Dmax(1,j+1,2)
            Dmax(1,j,1) = Dmax(1,j+1,2);
       end
   end
   
   if D(1,j,1) < D(1,j-1,2)
       if Dmax(1,j,1) < Dmax(1,j-1,2)
            Dmax(1,j,1) = Dmax(1,j-1,2);
       end
   end
   
    if D(1,j,1) < D(2,j-1,2)
       if Dmax(1,j,1) < Dmax(2,j-1,2)
            Dmax(1,j,1) = Dmax(2,j-1,2);
       end
    end
   
     if D(1,j,1) < D(2,j+1,2)
       if Dmax(1,j,1) < Dmax(2,j+1,2)
            Dmax(1,j,1) = Dmax(2,j+1,2);
       end
     end
   
     if D(1,j,1) < D(2,j,2)
       if Dmax(1,j,1) < Dmax(2,j,2)
            Dmax(1,j,1) = Dmax(2,j,2);
       end
     end
   
 %%
 
     if D(end,j,1) < D(end,j+1,1)
       if Dmax(end,j,1) < Dmax(end,j+1,1)
            Dmax(end,j,1) = Dmax(end,j+1,1);
       end
   end
   
   if D(end,j,1) < D(end,j-1,1)
       if Dmax(end,j,1) < Dmax(end,j-1,1)
            Dmax(end,j,1) = Dmax(end,j-1,1);
       end
   end
   
    if D(end,j,1) < D(end-1,j-1,1)
       if Dmax(end,j,1) < Dmax(end-1,j-1,1)
            Dmax(end,j,1) = Dmax(end-1,j-1,1);
       end
    end
   
     if D(end,j,1) < D(end-1,j+1,1)
       if Dmax(end,j,1) < Dmax(end-1,j+1,1)
            Dmax(end,j,1) = Dmax(end-1,j+1,1);
       end
     end
   
     if D(end,j,1) < D(end-1,j,1)
       if Dmax(end,j,1) < Dmax(end-1,j,1)
            Dmax(end,j,1) = Dmax(end-1,j,1);
       end
     end
   
      if D(end,j,1) < D(end,j,2)
       if Dmax(end,j,1) < Dmax(end,j,2)
            Dmax(end,j,1) = Dmax(end,j,2);
       end
      end
   
      if D(end,j,1) < D(end,j+1,2)
       if Dmax(end,j,1) < Dmax(end,j+1,2)
            Dmax(end,j,1) = Dmax(end,j+1,2);
       end
   end
   
   if D(end,j,1) < D(end,j-1,2)
       if Dmax(end,j,1) < Dmax(end,j-1,2)
            Dmax(end,j,1) = Dmax(end,j-1,2);
       end
   end
   
    if D(end,j,1) < D(end-1,j-1,2)
       if Dmax(end,j,1) < Dmax(end-1,j-1,2)
            Dmax(end,j,1) = Dmax(end-1,j-1,2);
       end
    end
   
     if D(end,j,1) < D(end-1,j+1,2)
       if Dmax(end,j,1) < Dmax(end-1,j+1,2)
            Dmax(end,j,1) = Dmax(end-1,j+1,2);
       end
     end
   
     if D(end,j,1) < D(end-1,j,2)
       if Dmax(end,j,1) < Dmax(end-1,j,2)
            Dmax(end,j,1) = Dmax(end-1,j,2);
       end
     end
   
     %% change in z
     
        if D(1,j,end) < D(1,j+1,end)
       if Dmax(1,j,end) < Dmax(1,j+1,end)
            Dmax(1,j,end) = Dmax(1,j+1,end);
       end
   end
   
   if D(1,j,end) < D(1,j-1,end)
       if Dmax(1,j,end) < Dmax(1,j-1,end)
            Dmax(1,j,end) = Dmax(1,j-1,end);
       end
   end
   
    if D(1,j,end) < D(2,j-1,end)
       if Dmax(1,j,end) < Dmax(2,j-1,end)
            Dmax(1,j,end) = Dmax(2,j-1,end);
       end
    end
   
     if D(1,j,end) < D(2,j+1,end)
       if Dmax(1,j,end) < Dmax(2,j+1,end)
            Dmax(1,j,end) = Dmax(2,j+1,end);
       end
     end
   
     if D(1,j,end) < D(2,j,end)
       if Dmax(1,j,end) < Dmax(2,j,end)
            Dmax(1,j,end) = Dmax(2,j,end);
       end
     end
   
       if D(1,j,end) < D(1,j,end-1)
       if Dmax(1,j,end) < Dmax(1,j,end-1)
            Dmax(1,j,end) = Dmax(1,j,end-1);
       end
      end
      if D(1,j,end) < D(1,j+1,end-1)
       if Dmax(1,j,end) < Dmax(1,j+1,end-1)
            Dmax(1,j,end) = Dmax(1,j+1,end-1);
       end
   end
   
   if D(1,j,end) < D(1,j-1,end-1)
       if Dmax(1,j,end) < Dmax(1,j-1,end-1)
            Dmax(1,j,end) = Dmax(1,j-1,end-1);
       end
   end
   
    if D(1,j,end) < D(2,j-1,end-1)
       if Dmax(1,j,end) < Dmax(2,j-1,end-1)
            Dmax(1,j,end) = Dmax(2,j-1,end-1);
       end
    end
   
     if D(1,j,end) < D(2,j+1,end-1)
       if Dmax(1,j,end) < Dmax(2,j+1,end-1)
            Dmax(1,j,end) = Dmax(2,j+1,end-1);
       end
     end
   
     if D(1,j,end) < D(2,j,end-1)
       if Dmax(1,j,end) < Dmax(2,j,end-1)
            Dmax(1,j,end) = Dmax(2,j,end-1);
       end
     end
   
 %%
 
     if D(end,j,end) < D(end,j+1,end)
       if Dmax(end,j,end) < Dmax(end,j+1,end)
            Dmax(end,j,end) = Dmax(end,j+1,end);
       end
   end
   
   if D(end,j,end) < D(end,j-1,end)
       if Dmax(end,j,end) < Dmax(end,j-1,end)
            Dmax(end,j,end) = Dmax(end,j-1,end);
       end
   end
   
    if D(end,j,end) < D(end-1,j-1,end)
       if Dmax(end,j,end) < Dmax(end-1,j-1,end)
            Dmax(end,j,end) = Dmax(end-1,j-1,end);
       end
    end
   
     if D(end,j,end) < D(end-1,j+1,end)
       if Dmax(end,j,end) < Dmax(end-1,j+1,end)
            Dmax(end,j,end) = Dmax(end-1,j+1,end);
       end
     end
   
     if D(end,j,end) < D(end-1,j,end)
       if Dmax(end,j,end) < Dmax(end-1,j,end)
            Dmax(end,j,end) = Dmax(end-1,j,end);
       end
     end
   
      if D(end,j,end) < D(end,j,end-1)
       if Dmax(end,j,end) < Dmax(end,j,end-1)
            Dmax(end,j,end) = Dmax(end,j,end-1);
       end
      end
   
      if D(end,j,end) < D(end,j+1,end-1)
       if Dmax(end,j,end) < Dmax(end,j+1,end-1)
            Dmax(end,j,end) = Dmax(end,j+1,end-1);
       end
   end
   
   if D(end,j,end) < D(end,j-1,end-1)
       if Dmax(end,j,end) < Dmax(end,j-1,end-1)
            Dmax(end,j,end) = Dmax(end,j-1,end-1);
       end
   end
   
    if D(end,j,end) < D(end-1,j-1,end-1)
       if Dmax(end,j,end) < Dmax(end-1,j-1,end-1)
            Dmax(end,j,end) = Dmax(end-1,j-1,end-1);
       end
    end
   
     if D(end,j,end) < D(end-1,j+1,end-1)
       if Dmax(end,j,end) < Dmax(end-1,j+1,end-1)
            Dmax(end,j,end) = Dmax(end-1,j+1,end-1);
       end
     end
   
     if D(end,j,end) < D(end-1,j,end-1)
       if Dmax(end,j,end) < Dmax(end-1,j,end-1)
            Dmax(end,j,end) = Dmax(end-1,j,end-1);
       end
     end
  end
  end
     
  %check k edge

  for k=2:Az-1
  
     if D(i,j,k) ~= 0
    
    if D(1,1,k) < D(1,1,k+1)
       if Dmax(1,1,k) < Dmax(1,1,k+1)
            Dmax(1,1,k) = Dmax(1,1,k+1);
       end
   end
   
   if D(1,1,k) < D(1,1,k-1)
       if Dmax(1,1,k) < Dmax(1,1,k-1)
            Dmax(1,1,k) = Dmax(1,1,k-1);
       end
   end
   
    if D(1,1,k) < D(1,2,k-1)
       if Dmax(1,1,k) < Dmax(1,2,k-1)
            Dmax(1,1,k) = Dmax(1,2,k-1);
       end
    end
   
     if D(1,1,k) < D(1,2,k+1)
       if Dmax(1,1,k) < Dmax(1,2,k+1)
            Dmax(1,1,k) = Dmax(1,2,k+1);
       end
     end
   
     if D(1,1,k) < D(1,2,k)
       if Dmax(1,1,k) < Dmax(1,2,k)
            Dmax(1,1,k) = Dmax(1,2,k);
       end
     end
   
       if D(1,1,k) < D(2,1,k)
       if Dmax(1,1,k) < Dmax(2,1,k)
            Dmax(1,1,k) = Dmax(2,1,k);
       end
      end
      if D(1,1,k) < D(2,1,k+1)
       if Dmax(1,1,k) < Dmax(2,1,k+1)
            Dmax(1,1,k) = Dmax(2,1,k+1);
       end
   end
   
   if D(1,1,k) < D(2,1,k-1)
       if Dmax(1,1,k) < Dmax(2,1,k-1)
            Dmax(1,1,k) = Dmax(2,1,k-1);
       end
   end
   
    if D(1,1,k) < D(2,2,k-1)
       if Dmax(1,1,k) < Dmax(2,2,k-1)
            Dmax(1,1,k) = Dmax(2,2,k-1);
       end
    end
   
     if D(1,1,k) < D(2,2,k+1)
       if Dmax(1,1,k) < Dmax(2,2,k+1)
            Dmax(1,1,k) = Dmax(2,2,k+1);
       end
     end
   
     if D(1,1,k) < D(2,2,k)
       if Dmax(1,1,k) < Dmax(2,2,k)
            Dmax(1,1,k) = Dmax(2,2,k);
       end
     end
   
 %%
 
     if D(1,end,k) < D(1,end,k+1)
       if Dmax(1,end,k) < Dmax(1,end,k+1)
            Dmax(1,end,k) = Dmax(1,end,k+1);
       end
   end
   
   if D(1,end,k) < D(1,end,k-1)
       if Dmax(1,end,k) < Dmax(1,end,k-1)
            Dmax(1,end,k) = Dmax(1,end,k-1);
       end
   end
   
    if D(1,end,k) < D(1,end-1,k-1)
       if Dmax(1,end,k) < Dmax(1,end-1,k-1)
            Dmax(1,end,k) = Dmax(1,end-1,k-1);
       end
    end
   
     if D(1,end,k) < D(1,end-1,k+1)
       if Dmax(1,end,k) < Dmax(1,end-1,k+1)
            Dmax(1,end,k) = Dmax(1,end-1,k+1);
       end
     end
   
     if D(1,end,k) < D(1,end-1,k)
       if Dmax(1,end,k) < Dmax(1,end-1,k)
            Dmax(1,end,k) = Dmax(1,end-1,k);
       end
     end
   
      if D(1,end,k) < D(2,end,k)
       if Dmax(1,end,k) < Dmax(2,end,k)
            Dmax(1,end,k) = Dmax(2,end,k);
       end
      end
   
      if D(1,end,k) < D(2,end,k+1)
       if Dmax(1,end,k) < Dmax(2,end,k+1)
            Dmax(1,end,k) = Dmax(2,end,k+1);
       end
   end
   
   if D(1,end,k) < D(2,end,k-1)
       if Dmax(1,end,k) < Dmax(2,end,k-1)
            Dmax(1,end,k) = Dmax(2,end,k-1);
       end
   end
   
    if D(1,end,k) < D(2,end-1,k-1)
       if Dmax(1,end,k) < Dmax(2,end-1,k-1)
            Dmax(1,end,k) = Dmax(2,end-1,k-1);
       end
    end
   
     if D(1,end,k) < D(2,end-1,k+1)
       if Dmax(1,end,k) < Dmax(2,end-1,k+1)
            Dmax(1,end,k) = Dmax(2,end-1,k+1);
       end
     end
   
     if D(1,end,k) < D(2,end-1,k)
       if Dmax(1,end,k) < Dmax(2,end-1,k)
            Dmax(1,end,k) = Dmax(2,end-1,k);
       end
     end
   
     %% change in z
     
      if D(end,1,k) < D(end,1,k+1)
       if Dmax(end,1,k) < Dmax(end,1,k+1)
            Dmax(end,1,k) = Dmax(end,1,k+1);
       end
   end
   
   if D(end,1,k) < D(end,1,k-1)
       if Dmax(end,1,k) < Dmax(end,1,k-1)
            Dmax(end,1,k) = Dmax(end,1,k-1);
       end
   end
   
    if D(end,1,k) < D(end,2,k-1)
       if Dmax(end,1,k) < Dmax(end,2,k-1)
            Dmax(end,1,k) = Dmax(end,2,k-1);
       end
    end
   
     if D(end,1,k) < D(end,2,k+1)
       if Dmax(end,1,k) < Dmax(end,2,k+1)
            Dmax(end,1,k) = Dmax(end,2,k+1);
       end
     end
   
     if D(end,1,k) < D(end,2,k)
       if Dmax(end,1,k) < Dmax(end,2,k)
            Dmax(end,1,k) = Dmax(end,2,k);
       end
     end
   
       if D(end,1,k) < D(end-1,1,k)
       if Dmax(end,1,k) < Dmax(end-1,1,k)
            Dmax(end,1,k) = Dmax(end-1,1,k);
       end
      end
      if D(end,1,k) < D(end-1,1,k+1)
       if Dmax(end,1,k) < Dmax(end-1,1,k+1)
            Dmax(end,1,k) = Dmax(end-1,1,k+1);
       end
   end
   
   if D(end,1,k) < D(end-1,1,k-1)
       if Dmax(end,1,k) < Dmax(end-1,1,k-1)
            Dmax(end,1,k) = Dmax(end-1,1,k-1);
       end
   end
   
    if D(end,1,k) < D(end-1,2,k-1)
       if Dmax(end,1,k) < Dmax(end-1,2,k-1)
            Dmax(end,1,k) = Dmax(end-1,2,k-1);
       end
    end
   
     if D(end,1,k) < D(end-1,2,k+1)
       if Dmax(end,1,k) < Dmax(end-1,2,k+1)
            Dmax(end,1,k) = Dmax(end-1,2,k+1);
       end
     end
   
     if D(end,1,k) < D(end-1,2,k)
       if Dmax(end,1,k) < Dmax(end-1,2,k)
            Dmax(end,1,k) = Dmax(end-1,2,k);
       end
     end
   
 %%
 
     if D(end,end,k) < D(end,end,k+1)
       if Dmax(end,end,k) < Dmax(end,end,k+1)
            Dmax(end,end,k) = Dmax(end,end,k+1);
       end
   end
   
   if D(end,end,k) < D(end,end,k-1)
       if Dmax(end,end,k) < Dmax(end,end,k-1)
            Dmax(end,end,k) = Dmax(end,end,k-1);
       end
   end
   
    if D(end,end,k) < D(end,end-1,k-1)
       if Dmax(end,end,k) < Dmax(end,end-1,k-1)
            Dmax(end,end,k) = Dmax(end,end-1,k-1);
       end
    end
   
     if D(end,end,k) < D(end,end-1,k+1)
       if Dmax(end,end,k) < Dmax(end,end-1,k+1)
            Dmax(end,end,k) = Dmax(end,end-1,k+1);
       end
     end
   
     if D(end,end,k) < D(end,end-1,k)
       if Dmax(end,end,k) < Dmax(end,end-1,k)
            Dmax(end,end,k) = Dmax(end,end-1,k);
       end
     end
   
      if D(end,end,k) < D(end-1,end,k)
       if Dmax(end,end,k) < Dmax(end-1,end,k)
            Dmax(end,end,k) = Dmax(end-1,end,k);
       end
      end
   
      if D(end,end,k) < D(end-1,end,k+1)
       if Dmax(end,end,k) < Dmax(end-1,end,k+1)
            Dmax(end,end,k) = Dmax(end-1,end,k+1);
       end
   end
   
   if D(end,end,k) < D(end-1,end,k-1)
       if Dmax(end,end,k) < Dmax(end-1,end,k-1)
            Dmax(end,end,k) = Dmax(end-1,end,k-1);
       end
   end
   
    if D(end,end,k) < D(end-1,end-1,k-1)
       if Dmax(end,end,k) < Dmax(end-1,end-1,k-1)
            Dmax(end,end,k) = Dmax(end-1,end-1,k-1);
       end
    end
   
     if D(end,end,k) < D(end-1,end-1,k+1)
       if Dmax(end,end,k) < Dmax(end-1,end-1,k+1)
            Dmax(end,end,k) = Dmax(end-1,end-1,k+1);
       end
     end
   
     if D(end,end,k) < D(end-1,end-1,k)
       if Dmax(end,end,k) < Dmax(end-1,end-1,k)
            Dmax(end,end,k) = Dmax(end-1,end-1,k);
       end
     end
   
     end
  end
%%
% the corners

 if D(1,1,1) < D(2,1,1)
       if Dmax(1,1,1) < Dmax(2,1,1)
            Dmax(1,1,1) = Dmax(2,1,1);
       end
     end

if D(1,1,1) < D(1,2,1)
       if Dmax(1,1,1) < Dmax(1,2,1)
            Dmax(1,1,1) = Dmax(1,2,1);
       end
     end

if D(1,1,1) < D(1,1,2)
       if Dmax(1,1,1) < Dmax(1,1,2)
            Dmax(1,1,1) = Dmax(1,1,2);
       end
     end


if D(end,1,1) < D(end-1,1,1)
       if Dmax(end,1,1) < Dmax(end-1,1,1)
            Dmax(end,1,1) = Dmax(end-1,1,1);
       end
     end

if D(end,1,1) < D(end,2,1)
       if Dmax(end,1,1) < Dmax(end,2,1)
            Dmax(end,1,1) = Dmax(end,2,1);
       end
     end

if D(end,1,1) < D(end,1,2)
       if Dmax(end,1,1) < Dmax(end,1,2)
            Dmax(end,1,1) = Dmax(end,1,2);
       end
     end

 if D(1,end,1) < D(2,end,1)
       if Dmax(1,end,1) < Dmax(2,end,1)
            Dmax(1,end,1) = Dmax(2,end,1);
       end
     end

if D(1,end,1) < D(1,end-1,1)
       if Dmax(1,end,1) < Dmax(1,end-1,1)
            Dmax(1,end,1) = Dmax(1,end-1,1);
       end
     end

if D(1,end,1) < D(1,end,2)
       if Dmax(1,end,1) < Dmax(1,end,2)
            Dmax(1,end,1) = Dmax(1,end,2);
       end
     end

if D(1,1,end) < D(2,1,end)
       if Dmax(1,1,end) < Dmax(2,1,end)
            Dmax(1,1,end) = Dmax(2,1,end);
       end
     end

if D(1,1,end) < D(1,2,end)
       if Dmax(1,1,end) < Dmax(1,2,end)
            Dmax(1,1,end) = Dmax(1,2,end);
       end
     end

if D(1,1,end) < D(1,1,end-1)
       if Dmax(1,1,end) < Dmax(1,1,end-1)
            Dmax(1,1,end) = Dmax(1,1,end-1);
       end
     end

if D(end,1,end) < D(end-1,1,end)
       if Dmax(end,1,end) < Dmax(end-1,1,end)
            Dmax(end,1,end) = Dmax(end-1,1,end);
       end
     end

if D(end,1,end) < D(end,2,end)
       if Dmax(end,1,end) < Dmax(end,2,end)
            Dmax(end,1,end) = Dmax(end,2,end);
       end
     end

if D(end,1,end) < D(end,1,end-1)
       if Dmax(end,1,end) < Dmax(end,1,end-1)
            Dmax(end,1,end) = Dmax(end,1,end-1);
       end
     end


if D(end,end,end) < D(end-1,end,end)
       if Dmax(end,end,end) < Dmax(end-1,end,end)
            Dmax(end,end,end) = Dmax(end-1,end,end);
       end
     end

if D(end,end,end) < D(end,end-1,end)
       if Dmax(end,end,end) < Dmax(end,end-1,end)
            Dmax(end,end,end) = Dmax(end,end-1,end);
       end
     end

if D(end,end,end) < D(end,end,end-1)
       if Dmax(end,end,end) < Dmax(end,end,end-1)
            Dmax(end,end,end) = Dmax(end,end,end-1);
       end
     end

if D(end,end,1) < D(end-1,end,1)
       if Dmax(end,end,1) < Dmax(end-1,end,1)
            Dmax(end,end,1) = Dmax(end-1,end,1);
       end
     end

if D(end,end,1) < D(end,end-1,1)
       if Dmax(end,end,1) < Dmax(end,end-1,1)
            Dmax(end,end,1) = Dmax(end,end-1,1);
       end
     end

if D(end,end,1) < D(end,end,2)
       if Dmax(end,end,1) < Dmax(end,end,2)
            Dmax(end,end,1) = Dmax(end,end,2);
       end
     end

if D(1,end,end) < D(2,end,end)
       if Dmax(1,end,end) < Dmax(2,end,end)
            Dmax(1,end,end) = Dmax(2,end,end);
       end
     end

if D(1,end,end) < D(1,end-1,end)
       if Dmax(1,end,end) < Dmax(1,end-1,end)
            Dmax(1,end,end) = Dmax(1,end-1,end);
       end
     end

if D(1,end,end) < D(1,end,end-1)
       if Dmax(1,end,end) < Dmax(1,end,end-1)
            Dmax(1,end,end) = Dmax(1,end,end-1);
       end
     end

  end

Dmax(D == 0) = 0;
disp('Finished calculating Dmax')


end

