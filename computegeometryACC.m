function [G] = computegeometryACC(G,Ax,Ay,Az)

 [x, y, z]=meshgrid(1:Ax, 1:Ay, 1:Az);
        
        G.cells.centroids=[x(:)-0.5 y(:)-0.5 z(:)-0.5];clear x y z
        G.cells.centroids=G.cells.centroids(G.cells.indexMap,:);
        G.cells.centroids=G.cells.centroids(:,[2,1,3]);
        G.cells.volumes=ones(numel(G.cells.indexMap),1);
        faceCoords1=G.nodes.coords(G.faces.nodes(1:4:end),:);
        faceCoords2=G.nodes.coords(G.faces.nodes(2:4:end),:);
        faceCoords3=G.nodes.coords(G.faces.nodes(3:4:end),:);
        faceCoords4=G.nodes.coords(G.faces.nodes(4:4:end),:);
        G.faces.centroids=(faceCoords1+faceCoords2+faceCoords3+faceCoords4)./4;


end
