

function [K] = TwoPhasePFVS(sample,R,P1,P2,Shape,rho,mu,G,w)
Ax = size(sample,1);
Ay = size(sample,2);
Az = size(sample,3);
w(sample == 1) = 0;
active = find(sample == 0);
G = extractSubgrid(G, active);
tic
G = computegeometryACC(G,Ax,Ay,Az);
        disp('time for G')
toc
%% Set rock and fluid data

disp('Set rock and fluid data')
rock = makeRock(G, 1.*darcy(), 1);
rock.perm = w(:);
rock.poro=ones(G.cells.num,1);
rock.perm=rock.perm(rock.perm>0);
rock.poro=ones(G.cells.num,1);
rock.poro=ones(G.cells.num,1);
tic
T         = computeCartTransACC(rock);
 disp('time for T')
 toc
gravity off;
fluid     = initSingleFluid('mu' ,    1000*centi*poise     , ...
                            'rho', 1*kilogram/meter^3);
                        
% Set boundary conditions
%%
disp('Set boundary conditions')
resSol = initResSol(G, 0.0);
faceIndexList=[1:G.faces.num]';        
bc=[];                 
bcFaces = G.faces.centroids(:,3)== 0; % find the centroids on the faces 
bc = addBC(bc, faceIndexList(bcFaces), 'pressure',P1);
bcFaces = G.faces.centroids(:,3)== Az; % find the centroids on the faces 
bc = addBC(bc, faceIndexList(bcFaces), 'pressure',P2);

% Solve the linear system
%%

disp('Solve')

mrstModule add agmg
tic
resSol = incompTPFA(resSol, G, T, fluid, 'bc', bc, ...
                    'LinSolve', @(A,b) agmg(A,b));
                toc
%%

disp('Calculate Perm')

dFluxCell=faceFlux2cellVelocity(G,resSol.flux);
dFluxCell(isnan(dFluxCell)) = 0;

xvel = dFluxCell(:,1)./R;
yvel = dFluxCell(:,2)./R;
zvel = dFluxCell(:,3)./R;

Lz = Az;
Lx = Ax;
Ly = Ay;

zVel2=zeros(Lz*Ly*Lx,1);
zVel2(G.cells.indexMap)=zvel;
zVel2=reshape(zVel2,[Lx Ly Lz]);

Area = R^2.*ones(Lz,1);
q = zeros(Lz,1);
tempuz = zeros(Lz,1);
for k = 1:Lz;
tempuz(k) = sum(sum(zVel2(:,:,k)));
end
q1 = tempuz(:).*Area;
meanq = mean(q1);
 K = (Lz/(Lx*Ly))*((meanq*1)/(P1-P2))/(R*1)*10^12; %Darcy

end 