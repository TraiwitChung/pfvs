%% Slicing
%slice through the image

sample = sandpack;

for i = 1:size(sample,3)
    i
    
    figure(1)
    temp = sample(:,:,i);
    imagesc(squeeze(temp));
    colorbar;
    drawnow
    
end


