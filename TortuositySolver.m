startup;
mrstModule add agmg incomp libgeometry;

%% this scrip is for tortuosity calculation

%% importing binary micro-CT image
R = 10e-6 %in m
mu = 1; %viscosity
P1 = 1; %inlet pressure
P2 = 0; %outlet pressure
Shapefactor = 1; %shape 1 is for granular porous media, 2 is flow in fracture(s)

load geopack.mat %load micro-CT image
sample = geopack;

%changing flow direction (default is Z direction)
     %sample = permute(sample,[1 3 2]); %Y direction
     %sample = permute(sample,[3 2 1]); %X direction

% dimension of the image
Ax = size(sample,1);
Ay = size(sample,2);
Az = size(sample,3);
%% connectivity check
% the isolated pore voxels (no connection from inlet to outlet) are removed
disp('connectivity check')
sample = connectivitycheck(sample);    
D = double(bwdist(sample,'euclidean'));

%% calculating local conductivity
disp('calculating conductivity')
% calculate w
w = ones(Ax,Ay,Az);
w(sample == 1) =0;

%% Define geometry && Process geometry
% Construct a Cartesian grid from the imported image
% Remove solid cells
% accelerated compute geometry, compute only Cells.centroids and faces.centroids
G = cartGrid([Ax, Ay, Az], [Ax, Ay, Az]);
active = find(sample == 0);
G = extractSubgrid(G, active);
G = computegeometryACC(G,Ax,Ay,Az);

%% Set rock and fluid data
disp('Set rock and fluid data')
rock = makeRock(G, 1.*darcy(), 1);
rock.perm = w(:);
rock.perm=rock.perm(rock.perm>0);
rock.poro=ones(G.cells.num,1);
T         = computeCartTransACC(rock);
gravity off;
fluid     = initSingleFluid('mu' ,    1000*centi*poise     , ...
                            'rho', 1014*kilogram/meter^3);
                        
%% Set boundary conditions
disp('Set boundary conditions')
resSol = initResSol(G, 0.0);
faceIndexList=[1:G.faces.num]';        
bc=[];                 
bcFaces = G.faces.centroids(:,3)== 0; % find the centroids on the faces 
bc = addBC(bc, faceIndexList(bcFaces), 'pressure',P1);
bcFaces = G.faces.centroids(:,3)== Az; % find the centroids on the faces 
bc = addBC(bc, faceIndexList(bcFaces), 'pressure',P2);

%% Solve the linear system

resSol = incompTPFA(resSol, G, T, fluid, 'bc', bc, ...
                    'LinSolve', @(A,b) agmg(A,b));
                         


%% Calculate permeability

disp('Tortuosity calc')

dFluxCell=faceFlux2cellVelocity(G,resSol.flux);
dFluxCell(isnan(dFluxCell)) = 0;

xvel = (1/R)*dFluxCell(:,1);
yvel = (1/R)*dFluxCell(:,2);
zvel = (1/R)*dFluxCell(:,3);


%flow in z direction
Lz = Az;
Lx = Ax;
Ly = Ay;

zVel2=zeros(Lz*Ly*Lx,1);
zVel2(G.cells.indexMap)=zvel;
zVel2=reshape(zVel2,[Lx Ly Lz]);


Area = (R)^2*ones(Lz,1);

q = zeros(Lz,1);

tempuz = zeros(Lz,1);


for k = 1:Lz
    
tempuz(k) = sum(sum(zVel2(:,:,k)));

end

E= tempuz(:).*Area;

meanE = mean(E);

rhoz = (meanE*Lz)/((Lx*Ly*R)*(P1-P2));
%foremation factor
Fz = 1/rhoz

%m = 1.1 % for an unconsolidated sand image
porosity = sum(sample(:)==0)/(Ax*Ay*Az);
% tortuosity
a = Fz * porosity ;
a = sqrt(a)



