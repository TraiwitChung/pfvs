function [Dmax] = calcDmax(D,parforArg)
    Nx=size(D,1);
    Ny=size(D,2);
    Nz=size(D,3);
    Dmax=D;
    %for each pass of the algorithm
    parfor (i=1:Nx,parforArg)
        for j=1:Ny
            for k=1:Nz
                if D(i,j,k)>0
                    %use original location
                    ii=i;
                    jj=j;
                    kk=k;
                    scan=true;
                    %assign dmax as the max of the adjacent cells
                    dr=1;
                    count=0;
                    while scan==true
                        dx=[max(ii-dr,1) min(ii+dr,Nx)];
                        dy=[max(jj-dr,1) min(jj+dr,Ny)];
                        dz=[max(kk-dr,1) min(kk+dr,Nz)];
                        
                        boundMask=[[(ii==1) (ii==Nx)];
                                   [(jj==1) (jj==Ny)];
                                   [(kk==1) (kk==Nz)]];
                        locdx=1:(dx(2)-dx(1)+1);
                        locdy=1:(dy(2)-dy(1)+1);
                        locdz=1:(dz(2)-dz(1)+1);

                        adj=D(dx(1):dx(2),dy(1):dy(2),dz(1):dz(2));
                        adjv=adj(:);
                        [~,I]=max(adjv);
                        [x,y,z]=ind2sub(size(adj),I);
                        M=adj(x,y,z);
                        %check if this max is larger than current cell
                        delta=D(ii,jj,kk)-M;
                        % if M is larger, then walk towards, else terminate
                        if delta<0
                              ii=ii-size(locdx,2)+1-1*boundMask(1,2)+x;
                              jj=jj-size(locdy,2)+1-1*boundMask(2,2)+y; 
                              kk=kk-size(locdz,2)+1-1*boundMask(3,2)+z;
                              count=count+1;
                        else
                            scan=false;
                            Dmax(i,j,k)=M;
                        end
                    end
                end
            end
        end
        disp(['Dmax calculated for slice ',num2str(i),' of ',num2str(Nx)]);
    end
%     disp(['Dmax algorithm passing into next iteration n = ',num2str(n)])
%     if n==0
%     recurse=false;
%     end
% end