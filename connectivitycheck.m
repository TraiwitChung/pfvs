

function [sample] = connectivitycheck(sample)

Ax = size(sample,1);
Ay = size(sample,2);
Az = size(sample,3);

        sample=~sample;
        CC=bwconncomp(sample,6);
        disp(['Identification complete: ',num2str(CC.NumObjects),' partitions identified'])
        % check each partition for end to end opening. if a partition is a
        %single part, and open on both sides, it must be connecte
        %throughout
        indexMap=[1:numel(sample)]';
      %  disp('Filtering...')
        for i=1:CC.NumObjects
            if numel(CC.PixelIdxList{i})<Az
                CC.PixelIdxList{i}=[];
            end
        end
        
        CC.PixelIdxList=CC.PixelIdxList(~cellfun('isempty',CC.PixelIdxList));
        if numel(CC.PixelIdxList(:))>0
            for i=1:numel(CC.PixelIdxList(:))
                [~,~,z]=ind2sub([Ax Ay Az],CC.PixelIdxList{i});
                numIn=sum(z==1);
                numOut=sum(z==Az);
                if numIn==0
                    CC.PixelIdxList{i}=[];
                elseif numOut==0
                    CC.PixelIdxList{i}=[];
                end
            end
        end
        
        CC.PixelIdxList=CC.PixelIdxList(~cellfun('isempty',CC.PixelIdxList));
        disp(['Removal complete: ',num2str(numel(CC.PixelIdxList(:))),' connected partitions identified'])
        CC.PixelIdxList=cell2mat(CC.PixelIdxList');
        sample(sample==0)=1;
        sample=sample(:);
        sample(CC.PixelIdxList)=0;
        sample=reshape(sample,[Ax Ay Az]);


end
