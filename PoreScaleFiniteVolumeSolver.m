clc
clear
startup;
mrstModule add agmg incomp libgeometry;

%% importing binary micro-CT image
R = 10e-6 %in m
mu = 1; %viscosity
P1 = 1; %inlet pressure
P2 = 0; %outlet pressure
Shapefactor = 1; %shape 1 is for granular porous media, 2 is flow in fracture(s)

load sandpack.mat %load micro-CT image
sample = sandpack;

%changing flow direction (default is Z direction)
     %sample = permute(sample,[1 3 2]); %Y direction
     %sample = permute(sample,[3 2 1]); %X direction

% dimension of the image
Ax = size(sample,1);
Ay = size(sample,2);
Az = size(sample,3);
%% connectivity check
% the isolated pore voxels (no connection from inlet to outlet) are removed
disp('connectivity check')
sample = connectivitycheck(sample);    
D = double(bwdist(sample,'euclidean'));

%% calculating local conductivity
disp('calculating local conductivity')
maxD = max(D(:)) 
% calculate Dmax
[Dmax] = DmaxCalc(D,maxD,Ax,Ay,Az,Shapefactor,R,mu);
% calculate w
w =  (Shapefactor) * R^2 * (1/(8*mu)) .* (2 .* (Dmax .* D) - (D).^2 );
w = reshape(w,Ax,Ay,Az);


%% Define geometry && Process geometry
% Construct a Cartesian grid from the imported image
% Remove solid cells
% accelerated compute geometry, compute only Cells.centroids and faces.centroids
G = cartGrid([Ax, Ay, Az], [Ax, Ay, Az]);
active = find(sample == 0);
G = extractSubgrid(G, active);
G = computegeometryACC(G,Ax,Ay,Az);

%% Set rock and fluid data
disp('Set rock and fluid data')
rock = makeRock(G, 1.*darcy(), 1);
rock.perm = w(:);
rock.perm=rock.perm(rock.perm>0);
rock.poro=ones(G.cells.num,1);
T         = computeCartTransACC(rock);
gravity off;
fluid     = initSingleFluid('mu' ,    1000*centi*poise     , ...
                            'rho', 1014*kilogram/meter^3);
                        
                        
%% plotting pressure map

figure(1)
plotCellData(G, rock.perm);
title('Local conductivity map', 'FontSize',20)
xlabel('x'), ylabel('y'), zlabel('z');
view(3); camproj perspective; axis tight;
colorbar
set(gcf,'color','w');
                        
                        
%% Set boundary conditions
disp('Set boundary conditions')
resSol = initResSol(G, 0.0);
faceIndexList=[1:G.faces.num];        
bc=[];                 
bcFaces = G.faces.centroids(:,3)== 0; % find the centroids on the faces 
bc = addBC(bc, faceIndexList(bcFaces), 'pressure',P1);
bcFaces = G.faces.centroids(:,3)== Az; % find the centroids on the faces 
bc = addBC(bc, faceIndexList(bcFaces), 'pressure',P2);

%% Solve the linear system

resSol = incompTPFA(resSol, G, T, fluid, 'bc', bc, ...
                    'LinSolve', @(A,b) agmg(A,b));
                         


%% Calculate permeability

disp('Calculate Permeability')

%converting face flux to cell velocity
dFluxCell=faceFlux2cellVelocity(G,resSol.flux);
dFluxCell(isnan(dFluxCell)) = 0;

%extract velocity into each of the direction
xvel = dFluxCell(:,1)./R;
yvel = dFluxCell(:,2)./R;
zvel = dFluxCell(:,3)./R;

%only consider at flow in z direction
zVel2=zeros(Az*Ay*Ax,1);
zVel2(G.cells.indexMap)=zvel;
zVel2=reshape(zVel2,[Ax Ay Az]);
Area = R^2.*ones(Az,1);

q = zeros(Az,1);
tempuz = zeros(Az,1);

%sum of velocity at each slice in z direction
for k = 1:Az;
    
tempuz(k) = sum(sum(zVel2(:,:,k)));

end

%flow rate at each slice in z direction
q1 = tempuz(:).*Area;

figure(2);plot(q1);
title('Flow rate', 'FontSize',10)
xlabel('z'), ylabel('q')

% reporting results
meanq = mean(q1);
  disp(['Flow rate is ',num2str(meanq),'  m^3/s'])
  %calculate absolute permeability using Darcy equation
K = (Az/(Ax*Ay))*mu*((meanq*1)/(P1-P2))/(R)*10^12; 
  disp(['Absolute Permeability is ',num2str(K),'  Darcy'])
  %calculate porosity
porosity = sum(sample(:)==0)/(Ax*Ay*Az);
  disp(['porosity is ',num2str(porosity)])



%% plotting pressure map

figure(3)
plotCellData(G, resSol.pressure);
title('Pressure Map', 'FontSize',20)
xlabel('x'), ylabel('y'), zlabel('z');
view(3); camproj perspective; axis tight;
colorbar
 set(gcf,'color','w');
